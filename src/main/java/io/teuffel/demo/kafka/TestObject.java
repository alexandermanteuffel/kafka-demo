package io.teuffel.demo.kafka;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class TestObject {
    private int id;
    private String message;
    private NestedObject nestedObject;
}
