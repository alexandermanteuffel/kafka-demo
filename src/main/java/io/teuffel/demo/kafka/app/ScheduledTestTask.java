package io.teuffel.demo.kafka.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.teuffel.demo.kafka.NestedObject;
import io.teuffel.demo.kafka.TestObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

@Slf4j
@Component
public class ScheduledTestTask {
    private final String topic = "test.topic";

    private final KafkaMessageProducer pub;

    public ScheduledTestTask(KafkaMessageProducer pub) {
        this.pub = pub;
    }

    @Scheduled(fixedDelay = 5000)
    public void task() throws JsonProcessingException {
        TestObject testMessage = new TestObject(ThreadLocalRandom.current().nextInt(1,10000), String.format("This is a test-message at %s", LocalDateTime.now()), new NestedObject(ThreadLocalRandom.current().nextInt(1, 300)));
        pub.publishMessage(topic, testMessage, UUID.randomUUID().toString());
    }

}