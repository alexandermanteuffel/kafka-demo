package io.teuffel.demo.kafka.app;

import java.time.Duration;
import java.util.Arrays;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import io.teuffel.demo.kafka.KafkaConsumerFactory;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaMessageConsumer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMessageConsumer.class);

    @Resource
    private KafkaConsumerFactory kafkaConsumerFactory;

    public void subscribeTo(String topic) {
        LOGGER.info("subscribeTo topic=" + topic);
        Consumer<String, String> msgConsumer = kafkaConsumerFactory.subscribe(topic);

        try {
            while (true) {
                ConsumerRecords<String, String> consumerRecord = msgConsumer
                        .poll(Duration.ofMillis(1000));

                consumerRecord.forEach(record -> {
                    LOGGER.info("Message Key:" + record.key());
                    LOGGER.info("Message Value:" + record.value());
                    LOGGER.info("Message Partition:" + record.partition());
                    LOGGER.info("Message Offset:" + record.offset());
                });

                msgConsumer.commitAsync();
            }
        } finally {
            msgConsumer.commitSync();
            msgConsumer.close();
        }
    }
}