package io.teuffel.demo.kafka.app;

import javax.annotation.Resource;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.teuffel.demo.kafka.JsonSerializer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.teuffel.demo.kafka.KafkaProducerFactory;

@Component
public class KafkaMessageProducer {
    private static final Logger LOGGER = LoggerFactory.getLogger(KafkaMessageConsumer.class);

    @Resource
    private KafkaProducerFactory kafkaProducerFactory;

    private final JsonSerializer jsonSerializer;

    public KafkaMessageProducer(JsonSerializer json) {
        this.jsonSerializer = json;
    }

    public void publishMessage(String topic, Object messageObject, String key) throws JsonProcessingException {

        String message = jsonSerializer.serialize(messageObject);

        if ((topic == null) || (topic.isEmpty()) || (message == null) || (message.isEmpty())) {
            return;
        }

        LOGGER.info("Send message '" + message + "' to " + topic);
        if (key == null) {
            kafkaProducerFactory.send(new ProducerRecord<String, String>(topic, message));
        } else {
            kafkaProducerFactory.send(new ProducerRecord<String, String>(topic, key, message));
        }

    }
}