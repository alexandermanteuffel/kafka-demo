package io.teuffel.demo.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class KafkaConfig {

    @Value("${io.teuffel.demo.kafka.bootStrapServers}")
    private String bootStrapServers;

    @Value("${io.teuffel.demo.kafka.consumer.group}")
    private String consumerGroup;

    @Bean
    public KafkaProducerFactory kafkaProducerFactory() {
        return new KafkaProducerFactory(bootStrapServers);
    }

    @Bean
    public KafkaConsumerFactory kafkaConsumerFactory() {
        return new KafkaConsumerFactory(bootStrapServers, consumerGroup);
    }

}
