package io.teuffel.demo.kafka;

import io.teuffel.demo.kafka.app.KafkaMessageConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class AppStartupRunner implements ApplicationRunner {
    private static final Logger LOG =
      LoggerFactory.getLogger(AppStartupRunner.class);

    private KafkaMessageConsumer consumer;

    public AppStartupRunner(KafkaMessageConsumer consumer){
        this.consumer = consumer;
    }
    @Override
    public void run(ApplicationArguments args) throws Exception {
        this.consumer.subscribeTo("test.topic");
    }
}