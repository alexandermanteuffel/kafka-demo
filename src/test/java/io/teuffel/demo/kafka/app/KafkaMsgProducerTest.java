package io.teuffel.demo.kafka.app;

import com.fasterxml.jackson.core.JsonProcessingException;
import io.teuffel.demo.kafka.ApplicationTests;
import io.teuffel.demo.kafka.NestedObject;
import io.teuffel.demo.kafka.TestObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationTests.class)
public class KafkaMsgProducerTest {

    @Autowired
    private KafkaMessageProducer pub;
    private final TestObject testMessage = new TestObject(123, "This is a testmessage", new NestedObject(321));

    @Test
    public void publishTestMessage() throws JsonProcessingException {
        pub.publishMessage("test.topic", testMessage, UUID.randomUUID().toString());
    }

    @Test
    public void publishTestCliMessage() throws JsonProcessingException {
        pub.publishMessage("test.cli-topic", testMessage, UUID.randomUUID().toString());
    }

}
