package io.teuffel.demo.kafka.app;

import io.teuffel.demo.kafka.ApplicationTests;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApplicationTests.class)
public class KafkaMsgConsumerTest {

    @Autowired
    private KafkaMessageConsumer consumer;

    @Test
    public void consumeTestTopic() {
        consumer.subscribeTo("test.topic");
    }

    @Test
    public void consumeCliTopic() {
        consumer.subscribeTo("test.cli-topic");
    }

}
