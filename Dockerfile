FROM maven:3.8.4-jdk-11-slim AS build
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app
RUN mvn -DskipTests -f /usr/src/app/pom.xml clean package

FROM openjdk:11-jdk-slim
COPY --from=build /usr/src/app/target/kafka-demo-1.0.0-SNAPSHOT.jar /usr/app/kafka-demo-1.0.0-SNAPSHOT.jar 
ENTRYPOINT ["java","-jar","/usr/app/kafka-demo-1.0.0-SNAPSHOT.jar"]
